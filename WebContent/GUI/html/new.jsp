<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@include file="index.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<!-- <meta name="author" content="Darko Bunic"/> -->
		<meta name="description" content="Drag and drop table content with JavaScript"/>
		<meta name="viewport" content="width=device-width, user-scalable=no"/><!-- "position: fixed" fix for Android 2.2+ -->
		<link rel="stylesheet" href="style.css" type="text/css" media="screen"/>
		<script type="text/javascript">
			var redipsURL = '/javascript/drag-and-drop-example-3/';
		</script>
		<script type="text/javascript" src="../header.js"></script>
		<script type="text/javascript" src="../redips-drag-min.js"></script>
		<script type="text/javascript" src="script2.js"></script>
		<title>Timetable</title>
	</head>

<body>
	<div id="main_container">
		<div id="redips-drag">
	
			<!-- left container (table with subjects) -->
			<div id="left">
					<table id="table1">
						<colgroup>
							<col width="190"/>
						</colgroup>
						<tbody>
							<tr><td class="redips-trash" title="Trash">Trash</td></tr>
							<% subjects(out , 6) ; %>
						</tbody>
					</table>
					
				</div><!-- left container -->
		
				<!-- right container -->
				<div id="right">
					<table id="table2">
						<colgroup>
							<col width="50"/>
							<col width="100"/>
							<col width="100"/>
							<col width="100"/>
							<col width="100"/>
							<col width="100"/>
						</colgroup>
						<tbody>
							<tr>
								<!-- if checkbox is checked, clone school subjects to the whole table row  -->
								<td class="redips-mark blank">
									<input id="week" type="checkbox" title="Apply school subjects to the week" checked/>
									<input id="report" type="checkbox" title="Show subject report"/>
								</td>
								<td class="redips-mark dark">Monday</td>
								<td class="redips-mark dark">Tuesday</td>
								<td class="redips-mark dark">Wednesday</td>
								<td class="redips-mark dark">Thursday</td>
								<td class="redips-mark dark">Friday</td>

							</tr>
							
							<% timetable(out , "8:00" , 1) ; %>
							<% timetable(out , "9:00" , 2) ; %>
							<% timetable(out , "10:00" , 3) ; %>
							<% timetable(out , "11:00" , 4) ; %>
							<% timetable(out , "12:00" , 5) ; %>
							<tr>
								<td class="redips-mark dark">01:00</td>
								<td class="redips-mark lunch" >Lunch</td>
								<td class="redips-mark lunch" >Lunch</td>
								<td class="redips-mark lunch" >Lunch</td>
								<td class="redips-mark lunch" >Lunch</td>
								<td class="redips-mark lunch" >Lunch</td>
							</tr>
							<% timetable(out , "02:00" , 7) ; %>
							<% timetable(out , "03:00" , 8) ; %>
							<% timetable(out , "04:00" , 9) ; %>
							
						</tbody>
					</table>
				</div><!-- right container -->
		
		</div>
	</div><!-- main container -->
</body>
</html>