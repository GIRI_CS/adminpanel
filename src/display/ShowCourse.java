package display;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
public class ShowCourse
{
     
    String error;
    List<Object> allData=new ArrayList<Object>();
     
    public String faculty_name,faculty_department,faculty_code,department,faculty_type,faculty_designation,mobile;
    Object obj=new Object();
 
    public void setObj(Object obj) {
        this.obj = obj;
    }
    public String getError() {
        return error;
    }
    public List getDb_Data()
    {
        int i=0;
        try
        {		Connection con;
                String qry;
                try {
      			  // Provide database Driver according to your database
      			   Class.forName("org.postgresql.Driver");
      			   
      			   // Provide URL, database and credentials according to your database 
      			   con= DriverManager.getConnection("jdbc:postgresql://172.16.40.26:5432/student?currentSchema=timetable", "student","student");
      		  } catch (Exception e) {
      			   e.printStackTrace();
      			   return null;
      		  }
      		  if(con != null){
      			  System.out.println("Connection created successfully....");
      		  }            
                Statement s = con.createStatement();                
                qry="SELECT f_name,department,f_code,f_type,designation,contact "+"from faculty ";
                ResultSet r=s.executeQuery(qry);
                while(r.next())
                {
                    DataFields d=new DataFields(r.getString(1), r.getString(2), r.getString(3),r.getString(4), r.getString(5),r.getString(6));                    
                    allData.add(i,d);
                    i++;
                }
                 
        }
        catch(Exception ex)
        {
                error="<b>Contact Administrator :</b><br/>" + ex;
                
                System.out.println("Your query is not working " + ex);
        }
       return allData;
    }
    public String getFacultyName()
    {
        this.faculty_name=((DataFields)obj).faculty_name;
        return this.faculty_name;
    }
    public String getDepartment() {
        this.department=((DataFields)obj).department;
        return this.department;
    }
 
    public String getFacultyCode() {
        this.faculty_code=((DataFields)obj).faculty_code;
        return this.faculty_code;
    }
    public String getFacultyType() {
        this.faculty_type=((DataFields)obj).faculty_type;
        return this.faculty_type;
    }
    public String getFacultyDesignation() {
        this.faculty_designation=((DataFields)obj).faculty_designation;
        return this.faculty_designation;
    }
    public String getMobileNo() {
        this.mobile=((DataFields)obj).mobile;
        return this.mobile;
    }
 
    public class DataFields
    {
    	private String faculty_name;
    	private String department;
    	private String faculty_code;
    	private String faculty_type;
    	private String faculty_designation;
    	private String mobile;
 
        public DataFields(String  faculty_name,String  department,String  faculty_code,String  faculty_type,String faculty_designation,String mobile)
        {
            this.faculty_name=faculty_name;
            this.department=department;
            this.faculty_code=faculty_code;
            this.faculty_type=faculty_type;
            this.faculty_designation=faculty_designation;
            this.mobile=mobile;
        }
    }
    
}